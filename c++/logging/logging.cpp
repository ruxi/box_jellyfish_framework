#include "logging.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/file.hpp>

namespace box_jellyfish { namespace logging {

void OutputOptionsManager::set_options(const LoggingOptions& logging_options)
{
    if (m_logging_options == logging_options)
    {
        return;
    }
    m_logging_options = logging_options;

    if (m_file_sink)
    {
        boost::log::core::get()->remove_sink(m_file_sink);
    }
    constexpr std::size_t file_rotation_size = 5 * 1024 * 1024; //TODO - get this from a config somewhere
    m_file_sink = boost::log::add_file_log(
        boost::log::keywords::file_name = m_logging_options.log_path / (m_logging_options.log_filename + "_log_%Y-%m-%d_%H-%M-%S.log"),
        boost::log::keywords::rotation_size = file_rotation_size,
        boost::log::keywords::auto_flush = true
    );


    boost::log::add_common_attributes();

    //TODO
    //boost::log::core::get()->set_filter(...);

    //TODO - do formatting and filtering
    //m_file_sink->set_formatter(...);
    //m_file_sink->set_filters(...);

    
    if (m_logging_options.log_path != "" && !boost::filesystem::exists(m_logging_options.log_path)) 
    {
        boost::filesystem::create_directories(m_logging_options.log_path);
    }

    boost::log::core::get()->set_logging_enabled(m_logging_options.is_enabled);
}

}}  //box_jellyfish::logging