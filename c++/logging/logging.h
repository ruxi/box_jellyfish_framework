#pragma once

#include "singleton.h"

#include <boost/log/core/core.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/basic_logger.hpp>
#include <boost/log/sources/channel_feature.hpp>
#include <boost/log/sources/channel_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>

//TODO: saw other implementations with boost::log::aux::light_rw_mutex
#include <boost/thread/shared_mutex.hpp>


/// Creates a global logger object
#define CREATE_GLOBAL_LOGGER(_severity) \
    CREATE_CATEGORY_LOGGER(_severity, "default")

#define CREATE_CATEGORY_LOGGER(_severity, _category) \
    BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT(_severity, ::box_jellyfish::logging::BoostLoggerWrapper) { \
        return ::box_jellyfish::logging::BoostLoggerWrapper( \
            ::boost::log::keywords::severity = ::box_jellyfish::logging::SeverityLevel::_severity, \
            ::boost::log::keywords::channel = _category); \
    }

namespace box_jellyfish{

namespace logging {

    enum class SeverityLevel : std::uint8_t
    {
        error,
        warning,
        info,
        trace
    };

    class BoostLoggerWrapper : public boost::log::sources::basic_composite_logger<
        char,
        BoostLoggerWrapper,
        boost::log::sources::multi_thread_model<boost::shared_mutex>,
        boost::log::sources::features<
        boost::log::sources::severity< logging::SeverityLevel >,
        boost::log::sources::channel< std::string >
        >
    >
    {
        BOOST_LOG_FORWARD_LOGGER_CONSTRUCTORS(BoostLoggerWrapper)
    };

    //---------------------------------------------------
    // defining the frontend
    //---------------------------------------------------

    struct LoggingOptions
    {
        boost::filesystem::path log_path;
        std::string log_filename;
        bool is_enabled;

        bool operator == (const LoggingOptions& rhs) const
        {
            return log_path == rhs.log_path &&
                   log_filename == rhs.log_filename &&
                   is_enabled == is_enabled;
        }
    };

    class OutputOptionsManager
    {
    public:
        void set_options(const LoggingOptions& logging_options);
        const LoggingOptions& get_current_options() { return m_logging_options; }

    private:
        LoggingOptions m_logging_options;

        /// The sinks are responsible with redirecting the logs to various
        /// forms of output - log files, cout, cerr etc.
        using FileSink = boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend>;
        boost::shared_ptr<FileSink> m_file_sink;
    };

    using LoggingManager = utils::Singleton<OutputOptionsManager>;


    CREATE_GLOBAL_LOGGER(error);
    CREATE_GLOBAL_LOGGER(warning);
    CREATE_GLOBAL_LOGGER(info);
    CREATE_CATEGORY_LOGGER(trace, "custom");

#define LOG_RECORD(_severity) \
    BOOST_LOG_SEV(::box_jellyfish::logging::_severity::get(), ::box_jellyfish::logging::SeverityLevel::_severity) 


}} //box_jellyfish::logging