#pragma once
#include <memory>

namespace box_jellyfish {
namespace utils {

template<typename T>
class Singleton 
{
public:
    static T& get() 
    {
        if (!s_instance) 
        {
            s_instance.reset(new T());
        }
        return *s_instance;
    }

    Singleton(Singleton& rhs) = delete;
    Singleton(Singleton&& rhs) = delete;
    Singleton& operator = (Singleton& rhs) = delete;
    Singleton&& operator = (Singleton&& rhs) = delete;

    ~Singleton() { s_instance = nullptr; }

private:
    Singleton() { s_instance = nullptr; }
    
    static std::unique_ptr<T> s_instance;
};

template<typename T> std::unique_ptr<T> Singleton<T>::s_instance;

}} //box_jellyfish::utils