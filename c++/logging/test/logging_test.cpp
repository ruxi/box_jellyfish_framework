#include "gtest/gtest.h"
#include "logging/logging.h"


using namespace box_jellyfish::logging;

TEST(logging_tests, logging)
{
    
    LoggingOptions logging_options;
    logging_options.is_enabled = true;
    logging_options.log_filename = "test";
    logging_options.log_path = "";

    auto& logger = LoggingManager::get();
    logger.set_options(logging_options);

    //TODO: intercept log records with a sink, and test everything is logged as expected
    LOG_RECORD(error) << "Logging an error";
}
