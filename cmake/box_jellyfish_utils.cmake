function (BJ_install_target target_name)
    # call with BJ_install_target(my_target_name header1.jh header2.h ...)
    # ${ARGN} holds the list of unexpected vars, i.e. the list of header files to install in this case
    set(CONFIGS 
        RelWithDebInfo
        Debug
        Release
        MinSizeRel
        )

    foreach(config_name ${CONFIGS})
        install (TARGETS ${target_name} 
                CONFIGURATIONS ${config_name}
                DESTINATION ${CMAKE_SOURCE_DIR}/install/${config_name}/lib
                )
        install (FILES ${ARGN} 
                CONFIGURATIONS ${config_name}
                DESTINATION ${CMAKE_SOURCE_DIR}/install/${config_name}/include)
    endforeach(config_name)    
    
    set(has_debug_info "$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>")
    if (has_debug_info)
        message(STATUS "GUGU4 --------------- should write the pdb")
    endif()
    
endfunction()
