from conans import ConanFile, CMake
from conans.errors import ConanException
import os


class BoxJellyfish(ConanFile):
    name = "BoxJellyfish"
    url = "GUGU complete here with git repo"
    description = "Box Jellyfish framework"
    settings = "os", "compiler", "build_type", "arch"
    requires = ("boost/1.69.0@conan/stable",
                "gtest/1.8.0@bincrafters/stable")
    license="open source"
    generators = "cmake"
    exports_sources = "cmake*","conanfile*","c++*"
    short_paths=True
    
    def build(self):
        # build the package to upload it to artifacotry
        cmake = CMake(self)
        # keep these for debugging the cmake build steps
        #self.run('echo Calling cmake %s %s'
        #         % (self.source_folder, cmake.command_line))
        #self.run('cmake %s %s'
        #         % (self.source_folder, cmake.command_line))
        #self.run("printf \\n\\n")
        #self.run("echo Calling cmake --build . %s --target install" % cmake.build_config)
        #self.run("cmake --build . %s --target install" % cmake.build_config)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        folder_name = str(self.settings.build_type)
        self.copy("*", dst="", src=os.path.join("install", folder_name))
           

    def package_info(self):
        self.cpp_info.libs = ["box-jellyfish-logging"]
    
    #def imports(self):
    #    # Copies all dll files from packages bin folder to my "bin" folder
    #    # TODO:
    #    # as soon as the framework will actively need a boost dll (or any dll coming from conan)
    #    # I'll have to sort out these paths. Technically these are going to a bin folder outside
    #    # the builddirectory. Then we have to point to these at runtime (tests will be executables)
    #    self.copy("*.dll", dst="../bin", src="bin") # From bin to ../bin
    #    self.copy("*.dylib*", dst="../bin", src="lib") # From lib to ../bin
    #    self.copy("*.exe*", dst="../bin", src="bin") # exes from bin to ../bin
