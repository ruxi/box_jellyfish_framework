set CONAN_USER_HOME=C:\code_utils\default_conan_folder

mkdir build
cd build
@where cmake > nul 2> nul
@if %ERRORLEVEL% NEQ 0 (
    echo Adding "C:\Program Files (x86)\CMake\bin" and "C:\Program Files\CMake\bin" to Path
    set "Path=C:\Program Files (x86)\CMake\bin;C:\Program Files\CMake\bin;%Path%"
)
cmake -A x64 -G "Visual Studio 15 2017" ..
@cd ..
@pause
