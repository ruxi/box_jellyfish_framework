# TODO!!!!!
# This versioning system is very vulnerable to the branch this deployment script is called from!
# When we deploy a certain version, we need to check if we are on the corresponding release branch.
# If we are not, then the version should include the branch name, and should only be a prototype (not stable)

export CONAN_USER_HOME=/c/code_utils/default_conan_folder

export CONAN_DEBUG_CONFIG="win64_debug"
export CONAN_RELEASE_CONFIG="win64_release"

export CONAN_CHANNEL="stable"
export CONAN_USER="box_jellyfish"
export BOX_JELLYFISH_VERSION=$(python scripts/get_version.py)

# Purge any existing local packages
conan remove BoxJellyfish/$BOX_JELLYFISH_VERSION/* -f

# Build local cache packages for the configs we support
conan create . BoxJellyfish/$BOX_JELLYFISH_VERSION@$CONAN_USER/$CONAN_CHANNEL -pr $CONAN_DEBUG_CONFIG
conan create . BoxJellyfish/$BOX_JELLYFISH_VERSION@$CONAN_USER/$CONAN_CHANNEL -pr $CONAN_RELEASE_CONFIG
# Upload it all to artifactory
conan user -p MauveStinger -r box-jellyfish-artifactory box_jellyfish_user
conan upload BoxJellyfish/* -r box-jellyfish-artifactory --all --force -c