import version_helpers as vh

print(vh.get_version(current=True, print_logs=False).version)