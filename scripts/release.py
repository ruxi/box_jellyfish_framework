"""
Run this to create a release branch. This will update the version of
cmake/box_jellyfish_version.cmake when creating a new branch.
TODO: we need to find a solution when we merge up from release.
"""
import subprocess
import os
from subprocess import CalledProcessError
from git import Repo
import version_helpers as vh

working_tree_dir = os.getcwd()
current_ver_filename = vh.get_version_filename(current=True)
bump_ver_filename = vh.get_version_filename(current=False)
    

repo = Repo(working_tree_dir)
assert not repo.bare
        
v = vh.get_version(current=False, print_logs=True)
# if the version has a patch number on master it means something has gone wrong! 
# Maybe someone checked in the version file from a release branch into master?! 
assert v.values['PATCH'] == 0

branch_name = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD'])
branch_name = branch_name.decode('utf-8').rstrip()
                
if branch_name != 'master':
    print('Current branch is not master, it is {}. Can only create release branches from master.'.format(branch_name))
    exit()


subprocess.check_output(['git', 'pull'])
master_hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('utf-8').rstrip()
print('Commit hash on master is {}'.format(master_hash))
    
    
def revert_changes():
    # TODO: use gitython for these commands
    # TODO: this does not clean master after a dodgy push because the branch is protected. Best to leave it like this?!
    subprocess.check_output(['git', 'reset', '--hard'])
    subprocess.check_output(['git', 'checkout', 'master'])
    subprocess.check_output(['git', 'branch', '-D', new_name])
    subprocess.check_output(['git', 'push', 'origin', '--delete', new_name])
    subprocess.check_output(['git', 'reset', '--hard', master_hash])
    
error = False
try:    
    v.values['MINOR'] = int(v.values['MINOR'])+1
    
    new_name = 'release/{}.{}'.format(v.values['MAJOR'], v.values['MINOR'])
    accept = input('Creating release branch: {} Accept? y/n '.format(new_name))
    if accept in ['y', 'yes']:
        # do the changes on master first (if anything goes wrong, we'll revert to the initial commit)
        print('Write changes to master')
        v.write_to_file(bump_ver_filename)
        repo.index.add([bump_ver_filename])
        repo.index.commit('Updating version number to {}'.format(v.version))
        print('Pushing changes on master')
        print(subprocess.check_output(['git', 'push']))
        
        result = subprocess.check_output(['git', 'checkout', '-b', new_name])
            
        # Change minor version on the release branch
        v.write_to_file(current_ver_filename)
        
        print('git diff:')
        diffs = repo.head.commit.diff(None, create_patch=True)  #HEAD against working tree
        for diff in diffs:
            print(diff)

        repo.index.add([current_ver_filename])
        print('git status:')
        status = repo.index.diff(repo.head.commit)
        for diff in status:
            print(diff)

        repo.index.commit('Updating version number to {}'.format(v.version))
        accept = input('\n\nPush selected changes? y/n ')
        if accept in ['y', 'yes']:
            print(subprocess.check_output(['git', 'push', '-u', 'origin', 'HEAD']))
        else:
            revert_changes()
        
        print('Successfully created branch {}. Checking out the branch.'.format(new_name))
        result = subprocess.check_output(['git', 'checkout', new_name])
        
except CalledProcessError as err:
    print("{}, {}".format(err.output, err.returncode))
except:
    revert_changes()
    error = True
    raise RuntimeError('Failed to create release branch with the needed versions. Reverted all changes.')
finally:
    # master has to be left in a clean state
    if (error):
        try:
            subprocess.check_output(['git', 'checkout', 'master'])    
            subprocess.check_output(['git', 'reset', '--hard', master_hash])
            # TODO: we need a proper commit with the reverted things...
            print('Something went wrong and we might need to undo the last commit on master (but it is protected). Please bring master to {}'.format(branch_name))
        except:
            print('Failed to bring master back to a clean state after an error. Ooops!')