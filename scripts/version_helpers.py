import re
import os

class Version:
    def __init__(self):
        self.values = {}
        
    def set_version(self, version_type, value):
        #print('setting version type to {}, {}'.format(version_type, value))
        if version_type in ['MAJOR', 'MINOR', 'PATCH']:
            self.values[version_type]=int(value)
    
    def is_valid(self):
        return 'MAJOR' and 'MINOR' and 'PATCH' in self.values

    @property
    def version(self):
        return '{}.{}.{}'.format(self.values['MAJOR'],self.values['MINOR'],self.values['PATCH'])

    def write_to_file(self, filename):
        with open(filename, 'w', newline='\n') as f:
            f.write('set(BOX_JELLYFISH_VERSION_MAJOR {})\n'.format(self.values['MAJOR']))
            f.write('set(BOX_JELLYFISH_VERSION_MINOR {})\n'.format(self.values['MINOR']))
            f.write('set(BOX_JELLYFISH_VERSION_PATCH {})\n'.format(self.values['PATCH']))
            f.write('set(BOX_JELLYFISH_VERSION ${BOX_JELLYFISH_VERSION_MAJOR}.${BOX_JELLYFISH_VERSION_MINOR}.${BOX_JELLYFISH_VERSION_PATCH})\n')


def get_version_filename(current):
    if current:
        name = 'box_jellyfish_current_version.cmake'
    else:
        name = 'box_jellyfish_version.cmake'
    return os.path.join('cmake', name)   

            
def get_version(current=True, print_logs=True):
    filename = get_version_filename(current)
    
    expression = re.compile('(set *\(BOX_JELLYFISH_VERSION_)([A-Z]+) ([0-9]+)(\))')
    v = Version()
    with open(filename,'r') as f:
        for line in f:
            m = expression.match(str(line))
            if m is not None:
                v.set_version(m.group(2), m.group(3))
    if not v.is_valid():
        if print_logs:
            raise RuntimeError('Failed to get program version. Aborting. Parsed: {}'.format(v.values))
        else:
            return 'Invalid_version'
    else:
        if print_logs:
            print('Current version set to {}'.format(v.version))
    return v


